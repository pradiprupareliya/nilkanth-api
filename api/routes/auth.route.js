const express = require('express'),
      router = express.Router();
const { check } = require('express-validator');

const validate = require('../middlewares/validate');
const authenticate = require('../middlewares/authenticate');

const User = require('../models/users.model');
const authController = require('../controllers/auth.controller');

// POST: auth/login.
router.post('/login', [
  check('email').normalizeEmail().isEmail().withMessage('Enter a valid email address'),
  check('password').notEmpty().withMessage('You password is required'),
], validate, authController.login);

// POST: auth/register.
router.post('/register', [
  check('email').normalizeEmail().isEmail().withMessage('Enter a valid email address')
    .isLength({max: 255})
    .custom(value => {
      return User.findOne({email: value}).then(user => {
        if (user) {
          return Promise.reject('E-mail already in use');
        }
      })
    }),
  check('username').trim().escape().notEmpty().withMessage('You username is required'),
  check('password').isLength({min: 6}).withMessage('Must be at least 6 chars long'),
  check('confirmPassword').isLength({min: 6}).withMessage('Must be at least 6 chars long')
    .custom((value, { req }) => {
      if (value !== req.body.password) {
        return Promise.reject('Password confirmation does not match password');
      } else {
        return true;
      }
    }),
  check('firstName').trim().escape().notEmpty().withMessage('You first name is required'),
  check('lastName').trim().escape().notEmpty().withMessage('You last name is required')
], validate, authController.register);

// GET: auth/logout.
router.get('/logout', authenticate, authController.logout);

// GET: auth/forgot-password.
router.post('/forgot-password', [
  check('email').normalizeEmail().isEmail().withMessage('Enter a valid email address'),
], validate, authController.forgotPassword);

// POST: auth/reset/:token.
router.post('/reset/:token', [
  check('password').isLength({min: 6}).withMessage('Must be at least 6 chars long'),
  check('confirmPassword').isLength({min: 6}).withMessage('Must be at least 6 chars long')
    .custom((value, { req }) => {
      if (value !== req.body.password) {
        return Promise.reject('Password confirmation does not match password');
      } else {
        return true;
      }
    }),
], validate, authController.resetPassword);

// POST: auth/refresh-token.
router.post('/refresh-token', [
  check('refreshToken').trim().escape().notEmpty().withMessage('Refresh Token is required'),
], validate, authController.refreshToken);

module.exports = router;