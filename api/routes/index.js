const express = require('express');
// const path = require('path');
// const jwt = require('jsonwebtoken');
const router = express.Router();

const authenticate = require('../middlewares/authenticate');

const authRoute = require('./auth.route');
const userRoute = require('./user.route');
const roleRoute = require('./role.route');

router.use('/auth', authRoute);
router.use('/user', authenticate, userRoute);
router.use('/role', authenticate, roleRoute);

module.exports = router;