const express = require('express');
const router = express.Router();
const userController = require('../controllers/user.controller');
const { check } = require('express-validator');
const validate = require('../middlewares/validate');

router.get('/', userController.usersList);

// GET: user/profile.
router.get('/profile', userController.getProfile);

router.post('/', [
    check('email').normalizeEmail().isEmail().withMessage('Enter a valid email address').isLength({max: 255}),
    check('username').trim().escape().notEmpty().withMessage('You username is required'),
    check('password').isLength({min: 6}).withMessage('Must be at least 6 chars long'),
    check('confirmPassword').isLength({min: 6}).withMessage('Must be at least 6 chars long')
      .custom((value, { req }) => {
        if (value !== req.body.password) {
          return Promise.reject('Password confirmation does not match password');
        } else {
          return true;
        }
      }),
    check('firstName').trim().escape().notEmpty().withMessage('You first name is required'),
    check('lastName').trim().escape().notEmpty().withMessage('You last name is required')
], validate, userController.insert);

router.get('/:id', userController.getById);

router.put('/update/:id', [
    check('email').normalizeEmail().isEmail().withMessage('Enter a valid email address').isLength({max: 255}),
    check('username').trim().escape().notEmpty().withMessage('You username is required'),
    check('firstName').trim().escape().notEmpty().withMessage('You first name is required'),
    check('lastName').trim().escape().notEmpty().withMessage('You last name is required')
], validate, userController.update);

router.delete('/:id', userController.delete);
// router.post('/upload', userController.upload);


// POST: user/change-password.
router.post('/change-password', [
  // check('old-password').isLength({min: 6}).withMessage('Must be at least 6 chars long'),
  check('password').isLength({min: 6}).withMessage('Must be at least 6 chars long'),
  check('confirmPassword').isLength({min: 6}).withMessage('Must be at least 6 chars long')
    .custom((value, { req }) => {
      if (value !== req.body.password) {
        return Promise.reject('Password confirmation does not match password');
      } else {
        return true;
      }
    })
], validate, userController.changePassword);

// PUT: user/profile.
router.put('/profile', userController.updateProfile);

module.exports = router;