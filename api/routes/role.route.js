const express = require('express');
const router = express.Router();
const roleController = require('../controllers/role.controller');
const { check } = require('express-validator');
const validate = require('../middlewares/validate');

router.get('/', roleController.rolesList);

router.post('/', [
    check('roleName').trim().escape().notEmpty().withMessage('roleName is required').isLength({max: 255}),
    check('description').trim().escape().isLength({max: 500}).withMessage('Maximum 500 chars allowed.'),
], validate, roleController.insert);

router.get('/:id', roleController.getById);

router.put('/update/:id', [
    check('roleName').trim().escape().notEmpty().withMessage('roleName is required').isLength({max: 255}),
    check('description').trim().escape().isLength({max: 500}).withMessage('Maximum 500 chars allowed.'),
], validate, roleController.update);

router.delete('/:id', roleController.delete);

module.exports = router;