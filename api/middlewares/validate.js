/**
 * This middleware is used to check if the express-validator middleware returns an error.
 * 
 * If so, it recreates the error object using the param and msg keys and returns the error.
 */

const {validationResult} = require('express-validator');

module.exports = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        let extractedErrors = {}; 
        errors.array().map((err) => extractedErrors[err.param] = err.msg);

        return res.status(422).json({errors: extractedErrors});
    }

    next();
};