const _ = require('lodash');
const Role = require('../models/role.model');
const roleService = require('../services/role.service');

// @route GET api/role
// @desc Returns all roles
exports.rolesList = async function (req, res) {
    const pageNumber = Number(req.query.pageNo) || 1;
    const limit = Number(req.query.items) || 10;
    const sortColumn = req.query.orderBy || 'createdAt';
    const order = req.query.order || 'desc';
    const search = req.query.search || '';
    
    const count = await Role.find().count();

    await roleService.getAll(search, sortColumn, order, pageNumber, limit).then(roles => {
        res.status(200).json({success: true, result: {
                count: count,
                roles
            }
        });
    })
    .catch(err => res.status(500).json({success: false, message: err.message}));
};

// @route POST api/role
// @desc Add a new role
exports.insert = async (req, res) => {
    try {
        const {roleName} = req.body;

        // Make sure this role doesn't already exist
        const role = await Role.findOne({roleName: {$regex : new RegExp(roleName, "i") }});

        if (role) return res.status(401).json({success: false, message: 'Role already exists'});

        const newRole = new Role(_.pick(req.body, ['roleName', 'description']));

        await roleService.insertUpdate(newRole).then(role => {
            res.status(201).json({success: true, role});
        })
        .catch(err => res.status(500).json({success: false, message: err.message}));
    } catch (error) {
        res.status(500).json({success: false, message: error.message})
    }
};

// @route GET api/role/{id}
// @desc Returns a specific role by Id
exports.getById = async (req, res) => {
    try {
        const role = await roleService.getById(req.params.id);

        if (!role) return res.status(401).json({success: false, message: 'Role does not exist'});

        res.status(200).json({success: true, role});
    } catch (error) {
        res.status(500).json({success: false, message: error.message})
    }
};

// @route PUT api/role/{id}
// @desc Update role details
exports.update = async (req, res) => {
    try {
        const update = _.pick(req.body, ['roleName', 'description']);
        const id = req.params.id;

        const role = await roleService.getById(id);
        if (!role) return res.status(401).json({success: false, message: 'Role does not exist'});

        if (role.roleName.toLowerCase() == "admin" || role.roleName.toLowerCase() == "superadmin") {
            return res.status(401).json({success: false, message: "You can't change a role name."});
        }

        role.roleName = update.roleName;
        role.description = update.description;
        await roleService.insertUpdate(role).then(role => {
            return res.status(200).json({success: true, role});
        }).catch(err => res.status(500).json({success: false, message: err.message}));
    } catch (error) {
        res.status(500).json({message: error.message});
    }
};

// @route DELETE api/role/{id}
// @desc Delete role
exports.delete = async (req, res) => {
    try {
        const id = req.params.id;
        
        const role = await roleService.getById(id);

        if (!role) return res.status(401).json({success: false, message: 'Role does not exist.'});

        if (role.roleName.toLowerCase() == "admin" || role.roleName.toLowerCase() == "superadmin") {
            return res.status(401).json({success: false, message: "You can't delete " + role.roleName + " role."});
        }

        await roleService.delete(id);
        res.status(200).json({success: true, message: 'Role has been deleted'});
    } catch (error) {
        res.status(500).json({success: false, message: error.message});
    }
};
