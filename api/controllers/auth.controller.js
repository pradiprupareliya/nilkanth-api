const User = require('../models/users.model');
const Token = require('../models/token.model');

const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

// @route POST api/auth/register
// @desc Register user
// @access Public
exports.register = async (req, res) => {
    // Make sure this account doesn't already exist
    await User.findOne({email: req.body.email})
        .then(user => {

            if (user) return res.status(401).json({success: false, message: 'The email address you have entered is already associated with another account.'});

            // Create and save the user
            const newUser = new User(req.body);
            newUser.save()
                .then(user => {
                    result = user.toJSON();
                    delete result.password;

                    let refreshToken = new Token().generateRefreshToken(user._id);
                    let accessToken = user.generateAccessToken(user._id);

                    res.status(200).json({success: true, accessToken: accessToken, refreshToken: refreshToken, user: result, expiresIn: process.env.JWT_EXPIRATION});
                })
                .catch(err => res.status(500).json({success: false, message:err.message}));
        })
        .catch(err => res.status(500).json({success: false, message: err.message}));
};

// @route POST api/auth/login
// @desc Login user and return JWT token
// @access Public
exports.login = async (req, res) => {
    await User.findOne({email: req.body.email})
        .then(user => {
            if (!user) return res.status(401).json({success: false, msg: 'The email address is not available.'});

            // Validate password
            if (!user.comparePassword(req.body.password)) return res.status(401).json({success: false, message: 'Invalid email or password'});

            // Login successful, write token, and send back user
            if (user.image) {
                user.image = "images/" + user.image;
            }
            result = user.toJSON();
            delete result.password;

            let refreshToken = new Token().generateRefreshToken(user._id);
            let accessToken = user.generateAccessToken(user._id);

            res.status(200).json({success: true, accessToken: accessToken, refreshToken: refreshToken, user: result, expiresIn: process.env.JWT_EXPIRATION});
        })
        .catch(err => res.status(500).json({success: false, message: err.message}));
};

// @route POST api/auth/refresh-token
// @desc return JWT token
// @access Public
exports.refreshToken = async (req, res) => {
    let token = await Token.findOne({token: req.body.refreshToken})
        .then(token => {
            if (!token) return res.status(401).json({success: false, msg: 'Token is not available.'});

            // Validate password
            if (token.expired.getTime() < new Date().getTime()) return res.status(401).json({success: false, message: 'Token is expired.'});

            // NOTE:: you can update RefreshToken here.
            
            
            // Write token, and send back user
            return token;
        })
        .catch(err => res.status(500).json({success: false, message: err.message}));

    // Remove current user's extra token
    const extraCurrentUserToken = await Token.deleteMany({
        $and: [
            {userId: token.userId }, 
            {_id: { $ne: token._id } }
        ] 
    });
    
    let accessToken = new User().generateAccessToken(token.userId);
    res.status(200).json({success: true, accessToken: accessToken, expiresIn: process.env.JWT_EXPIRATION});
};

// @route POST api/auth/logout
// @desc destory a refreh tokens from database.
// @access Public
exports.logout = async (req, res) => {
    const result = await Token.deleteMany({
        $and: [
            {userId: req.user._id }, 
            // {_id: { $ne: token._id } }
        ] 
    })
    .catch(err => res.status(500).json({success: false, message: err.message}));

    if (result.deletedCount > 0) {
        return res.status(200).json({success: true, message: 'User is successfully logout.'});
    }

    return res.status(401).json({success: false, msg: 'Token is not deleted.'});
};

// @route POST api/auth/forgot-password
// @desc Recover Password - Generate token and sends password reset email
exports.forgotPassword = async (req, res) => {
    // Make sure this account doesn't already exist
    await User.findOne({email: req.body.email})
        .then(user => {

            if (!user) return res.status(401).json({success: false, message: 'The email address is not available.'});

            // Generate and set password reset token
            user.generatePasswordReset();

            user.save().then(user => {
                // send email
                let link = "http://" + req.headers.host + "/api/auth/reset/" + user.resetPasswordToken;
                const mailOpt = {
                    to: user.email,
                    from: process.env.FROM_EMAIL,
                    subject: "Password change request",
                    text: `Hi ${user.firstName},\n\n` + 
                        `Please click on the following link ${link} to reset your password \n\n`+
                        `If you did not request this, please ignore this email and your password will remain unchanged. \n`,
                };

                sgMail.send(mailOpt, (error, result) => {
                    if (error) return res.status(500).json({success: false, message: error.message});
                    
                    res.status(200).json({success: true, message: 'A reset email has been sent to ' + user.email + '.'});
                });
            })
            .catch(err => res.status(500).json({success: false, message:err.message}));
            
        })
        .catch(err => res.status(500).json({success: false, message: err.message}));
};

// @route POST api/auth/reset/:token
// @desc Reset Password
exports.resetPassword = async (req, res) => {
    // Make sure this account doesn't already exist
    await User.findOne({resetPasswordToken: req.params.token, resetPasswordExpires: {$gt: Date.now()}})
        .then(user => {

            if (!user) return res.status(401).json({success: false, message: 'Password reset token is invalid or has expired.'});

            user.password = req.body.password;
            user.resetPasswordToken = undefined;
            user.resetPasswordExpires = undefined;

            // Save
            user.save((err) => {
                if (err) return res.status(500).json({success: false, message: err.message});

                res.status(200).json({success: true, message: 'Your password has been updated.'});
            });
        })
        .catch(err => res.status(500).json({success: false, message: err.message}));
};
