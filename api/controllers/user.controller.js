const User = require('../models/users.model');
const formidable = require('formidable');
const fs = require('fs');
const { uuid } = require('uuidv4');
const path = require('path');

// @route GET api/user
// @desc Returns all users
// @access Public
exports.usersList = async function (req, res) {
    const pageNumber = Number(req.query.pageNo) || 1;
    let limit = Number(req.query.items) || 10;
    const skip = (pageNumber - 1) * limit;
    const orderBy = req.query.orderBy || 'created_at';
    const order = Number(req.query.order) || 1;

    // const searchQuery = req.query.s || '';

    const users = await User.find({}).populate('roles', 'roleName');
    res.status(200).json({users});
};

// @route POST api/user
// @desc Add a new user
exports.insert = async (req, res) => {
    try {
        const {email} = req.body;

        // Make sure this account doesn't already exist
        const user = await User.findOne({email});

        if (user) return res.status(401).json({success: false, message: 'The email address you have entered is already available.'});

        const newUser = new User(req.body);

        const user_ = await newUser.save().then(user => {
            result = user.toJSON();
            delete result.password;

            res.status(201).json({success: true, user: result});
        })
        .catch(err => res.status(500).json({success: false, message:err.message}));
    } catch (error) {
        res.status(500).json({success: false, message: error.message})
    }
};

// @route GET api/user/{id}
// @desc Returns a specific user by Id
exports.getById = async (req, res) => {
    try {
        const id = req.params.id;

        const user = await User.findById(id).select('-password -__v');
        if (user.image) {
            user.image = "images/" + user.image;
        }

        if (!user) return res.status(401).json({success: false, message: 'User does not exist'});

        res.status(200).json({success: true, user});
    } catch (error) {
        res.status(500).json({success: false, message: error.message})
    }
};

// @route PUT api/user/{id}
// @desc Update user details
exports.update = async (req, res) => {
    try {
        const update = req.body;
        const id = req.params.id;
        const userId = req.user._id;

        //Make sure the passed id is that of the logged in user
        // if (userId.toString() === id.toString()) return res.status(401).json({success: false, message: "Sorry, you don't have the permission to update this data."});

        const user = await User.findById(id);
        if (!user) return res.status(401).json({success: false, message: 'User does not exist'});

        const emailExist = await User.findOne({
                            $and: [
                                {email: update.email},
                                {_id: { $ne: user._id }}
                            ] 
                        }).select('firstName -_id');
        if (emailExist) {
            return res.status(401).json({success: false, message: "Email already exist."});
        } else {
            user.email = update.email;
        }

        const usernameExist = await User.findOne(
            {
                $and: [
                    {username: update.username},
                    {_id: { $ne: user._id }}
                ] 
            }).select('firstName -_id');

        if (usernameExist) {
            return res.status(401).json({success: false, message: "Username already exist."});
        } else {
            user.username = update.username;
        }
        
        user.firstName = update.firstName;
        user.lastName = update.lastName;
        const updatedUser = await User.findByIdAndUpdate(id, {$set: user}, {new: true}).select('-password -__v');

        if (updatedUser.image) {
            updatedUser.image = "images/" + updatedUser.image;
        }

        //if there is no image, return success message
        if (!req.file) return res.status(200).json({success: true, user: updatedUser});

        //Attempt to upload to cloudinary
        // const result = await uploader(req);
        // const user_ = await User.findByIdAndUpdate(id, {$set: update}, {$set: {profileImage: result.url}}, {new: true});

        // if (!req.file) return res.status(200).json({user: user_, message: 'User has been updated'});

    } catch (error) {
        res.status(500).json({message: error.message});
    }
};

// @route DELETE api/user/{id}
// @desc Delete User
exports.delete = async (req, res) => {
    try {
        const id = req.params.id;
        
        const user = await User.findById(id).select('_id');
        if (!user) return res.status(401).json({success: false, message: 'User does not exist.'});

        await User.findByIdAndDelete(id);
        res.status(200).json({success: true, message: 'User has been deleted'});
    } catch (error) {
        res.status(500).json({success: false, message: error.message});
    }
};


// @route DESTROY api/user/change-password
// @desc Change a password of current user.
exports.changePassword = async (req, res) => {
    try {
        await User.findById(req.user.id).then(user => {
                if (!user) return res.status(401).json({success: false, message: 'User not found'});

                user.password = req.body.password;
                user.resetPasswordToken = undefined;
                user.resetPasswordExpires = undefined;

                // Save
                user.save((err) => {
                    if (err) return res.status(500).json({success: false, message: err.message});

                    res.status(200).json({success: true, message: 'Your password has been updated.'});
                });
            })
            .catch(err => res.status(500).json({success: false, message: err.message}));

        // const user = await User.findById(id);
        // if (!user) return res.status(401).json({success: false, message: 'User does not exist.'});

        // await User.findByIdAndDelete(id);
        // res.status(200).json({success: true, message: 'User has been deleted'});
    } catch (error) {
        res.status(500).json({success: false, message: error.message});
    }
};

// @route DESTROY api/user/profile
// @desc get current user details for profile page.
exports.getProfile = async (req, res) => {
    await User.findById(req.user._id).select('-password -__v').then(user => {
        if (!user) return res.status(401).json({success: false, message: 'User not found'});

        if (user.image) {
            user.image = "images/" + user.image;
        }

        res.status(200).json({success: true, user});
    })
    .catch(err => res.status(500).json({success: false, message: err.message}));
};

// @route PUT api/user/profile
// @desc Update profile details
exports.updateProfile = (req, res) => {
    try {
        const id = req.user._id;

        const form = formidable.IncomingForm();
        form.parse(req, (err, fields, files) => {
            if (err) {
                next(err);
                return;
            }

            const validationErrors = {};

            if (!fields.email) validationErrors["email"] = 'Enter a valid email address';
            if (fields.email.length > 255) validationErrors["email"] = 'Maximum 255 characters are allowed.';
            const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!re.test(String(fields.email).toLowerCase())) validationErrors["username"] = 'Email address is invalid';
            if (!fields.username) validationErrors["username"] = 'You username is required';
            if (!fields.firstName) validationErrors["firstName"] = 'You first name is required';
            if (!fields.lastName) validationErrors["lastName"] = 'You last name is required';

            if (Object.keys(validationErrors).length > 0) {
                return res.status(422).json({errors: validationErrors});
            }

            const user = User.findById(id).then(user => {
                if (!user) return res.status(401).json({success: false, message: 'User does not exist'});

                // const emailExist = User.findOne(
                //     {
                //         $and: [
                //             {email: fields.email},
                //             {_id: { $ne: user._id }}
                //         ] 
                //     }, async (err, emailExist) => {
                //         if (emailExist) {
                //             return res.status(401).json({success: false, message: "Email already exist."});
                //         } else {
                            user.email = fields.email;
                    //     }
                    // });


                // const usernameExist = User.findOne(
                //         {
                //             $and: [
                //                 {username: fields.username},
                //                 {_id: { $ne: user._id }}
                //             ] 
                //         }, async (err, usernameExist) => {
                //             if (usernameExist) {
                //                 return res.status(401).json({success: false, message: "Username already exist."});
                //             } else {
                                user.username = fields.username;
                        //     }
                        // });

                user.firstName = fields.firstName;
                user.lastName = fields.lastName;
    
                if (files && files.image != null) {
                    // Upload Image
                    originalFileName = files.image.name;
                    const newFileName = `${uuid()}_${originalFileName}`;
                    const fileRelativePath = `assets/images/${newFileName}`;
        
                    const oldPath = files.image.path;
                    const newPath = path.join(__dirname, '/../../' + fileRelativePath);
                    const fileData = fs.readFileSync(oldPath);
        
                    if (fileData) {
                        fs.writeFileSync(newPath, fileData);
        
                        // Delete the file
                        fs.unlink(oldPath, (err) => {
                            if (err) throw err;

                            console.log('File deleted!');
                        });
        
                        if (user.image) {
                            fs.unlink(`assets/images/${user.image}`, (err) => {
                                if (err) throw err;
                            });
                        }

                        user.image = newFileName;
                    } else {
                        res.status(400).json({success: false, message: 'Image not found'});
                    }
                }
                
                user.save((err, updatedUser) => {
                    if (err) {
                        return res.status(400).json({success: true, message: err.message});
                    }

                    if (updatedUser.image) {
                        updatedUser.image = "images/" + updatedUser.image;
                    }

                    result = updatedUser.toJSON();
                    delete result.password;
                    delete result.__v;
                    
                    return res.status(200).json({success: true, user: result});
                });
            });
        });

    } catch (error) {
        res.status(500).json({message: error.message});
    }
};