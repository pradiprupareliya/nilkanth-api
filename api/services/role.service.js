const Role = require('../models/role.model');

module.exports = {
    getAll,
    getById,
    insertUpdate,
    delete: _delete
};

// Get All Roles details.
async function getAll(search, sortColumn, order, pageNumber, limit) {
    const searchRegEx = new RegExp(`.*${search}.*`, 'i');

    const totalCount = await Role.find().count();

    if (limit == -1) {
        limit = totalCount;
    }

    return await Role.find({ roleName: searchRegEx })
        .select('_id roleName description')
        .sort([[sortColumn, order]])
        .skip((pageNumber - 1) * limit)
        .limit(limit);
        // .exec((err, roleList) => {
        //     if (err) {
        //         err;
        //     } else {
        //         roleList;
        //     }
        // });
}

// Insert/Update Role details.
async function insertUpdate(role) {
    if (role.isNew) {
        // Insert
        const newRole = new Role(role);
        return await newRole.save();
    } else {
        // Update
        return await Role.findByIdAndUpdate(role.id, {$set: role}, {new: true}).select('-__v');
    }
}

// Get Role details by Id.
async function getById(id) {
    return await Role.findById(id).select('_id roleName description');
}

// Delete Role by Id.
async function _delete(id) {
    return await Role.findByIdAndDelete(id);
}
