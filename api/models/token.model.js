const mongoose = require('mongoose');

const randtoken = require('rand-token');

// Define collection and schema for Items
const tokenSchema = new mongoose.Schema({
    userId: {
        type: mongoose.ObjectId,
        // required: true,
        trim: true,
        ref: 'users'
    },
    token: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    expired: {
        type: Date,
        required: true,
    },
}/*,{
    collection: 'users'
}*/, {timestamps: true});


tokenSchema.methods.generateRefreshToken = function(userId) {
    const token = this;

    var now = new Date();
    now.setDate(now.getDate() + 60);        // Add 60 days to current datetime.

    let refreshToken = randtoken.uid(48);

    token.userId = userId;
    token.token = refreshToken;
    token.expired = now;

    token.save();

    return refreshToken;
};

mongoose.set('useFindAndModify', false);
module.exports = mongoose.model('Token', tokenSchema);