const mongoose = require('mongoose');
// const bcrypt = require('bcrypt');
// const jwt = require('jsonwebtoken');
// const crypto = require('crypto');

// Define collection and schema for Items
const roleSchema = new mongoose.Schema({
    roleName: {
        type: String,
        unique: true,
        required: 'Your role is required',
        maxlength: 255,
        trim: true,
    },
    description: {
        type: String,
        trim: true,
        maxlength: 500
    }
}/*,{
    collection: 'users'
}*/, {timestamps: true});


roleSchema.pre('save', async function(next) {
    const role = this;

    // if (!user.isModified('password')) return next();

    // const salt = await bcrypt.genSalt(10);
    // user.password = await bcrypt.hash(user.password, salt);

    next();
});


module.exports = mongoose.model('roles', roleSchema);