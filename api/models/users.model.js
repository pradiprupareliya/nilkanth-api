const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');

// Define collection and schema for Items
const usersSchema = new mongoose.Schema({
    email: {
        type: String,
        unique: true,
        required: 'Your email is required',
        minlength: 1,
        maxlength: 255,
        trim: true,
        lowercase: true,
    },
    username: {
        type: String,
        unique: true,
        required: 'Your username is required',
        trim: true
    },
    password: {
        type: String,
        required: 'Your password is required',
        maxlength: 255,
    },
    firstName:  {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 50
    },
    lastName:  {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 50
    },
    image:  {
        type: String
    },
    roles: [{
        type: mongoose.ObjectId,
        trim: true,
        ref: 'roles'
    }],
    resetPasswordToken:  {
        type: String,
        required: false,
    },
    resetPasswordExpires:  {
        type: Date,
        required: false,
    },
}/*,{
    collection: 'users'
}*/, {timestamps: true});


usersSchema.pre('save', async function(next) {
    const user = this;

    if (!user.isModified('password')) return next();

    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);

    next();
});

usersSchema.methods.comparePassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

usersSchema.methods.generateAccessToken = function(userId) {
    let payload = {
        id: userId,
        // email: this.email,
        // username: this.username,
        // firstName: this.firstName,
        // lastName: this.lastName,
    };

    return jwt.sign(payload, process.env.JWT_SECRET, {
        expiresIn: parseInt(process.env.JWT_EXPIRATION)
    });
};

usersSchema.methods.generatePasswordReset = function() {
    this.resetPasswordToken = crypto.randomBytes(20).toString('hex');
    this.resetPasswordExpires = Date.now() + 3600000;           // expires in one hour
};

mongoose.set('useFindAndModify', false);
module.exports = mongoose.model('users', usersSchema);