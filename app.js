require('dotenv').config();

const express = require('express');
const bodyParser = require('body-parser');
const errorHandler = require('errorhandler');
const mongoose = require('mongoose');
const logger = require('morgan');
const cors = require('cors');
const passport = require("passport");
const path = require('path');


mongoose.Promise = global.Promise;
const connectionString = 'mongodb://'+ process.env.DB_HOST + ':' + process.env.DB_PORT + '/' + process.env.DB_DATABASE;
mongoose.connect(connectionString, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false })
.then(() => { console.log("✓ Database is connected  😍") })
.catch(err => console.error('Can not connect to the database:: '+ err));

const routes = require('./api/routes');


// Init App
const app = express();


app.use(logger('dev'));
app.use(express.json());                                    // parsing application/json
app.use(bodyParser.urlencoded({ extended: true }));         // parsing x-ww-form-urlencoded request bodies
app.use(cors());
app.use(express.static(path.join(__dirname, 'assets')));


//=== 3 - INITIALIZE PASSPORT MIDDLEWARE
app.use(passport.initialize());
require("./api/middlewares/jwt")(passport);

// API routes
app.get('/', (req, res) => res.send('Hello World !!'));
app.use('/api', routes);
// The catch all route
app.all('*', (req, res) => {
    res.status(404).send({msg: 'not found'});
});


// Error Handler.
if (process.env.APP_ENV === 'development') {
    // only use in development
    app.use(errorHandler());
} else {
    app.use((err, req, res, next) => {
        console.error(err);
        res.status(500).send('Server Error');
    });
}

// Start Express Server
const port = process.env.PORT || 3000;                  // PORT
app.listen(port, () => { 
    console.log(`✓ App is running at `+ process.env.APP_URL +`:%d in %s mode 🙂`, port, process.env.APP_ENV);
    console.log('  Press CTRL-C to stop\n');
}).on('error', (err) => {
    switch (err.code) {
        case 'EACCES':
          console.error(`Port ${port} requires elevated privileges`);
          process.exit(1);
        case 'EADDRINUSE':
          console.error(`Port ${port} is already in use`);
          process.exit(1);
        default:
          throw error;
      }
});   